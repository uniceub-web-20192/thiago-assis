
package main


import ( 
		"time"
		"net/http"
		"fmt"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.127:8092",
			IdleTimeout: duration,
		//	Handler    : 
		}

	http.HandleFunc("/", metodoEUrl)

	server.ListenAndServe()
}

func metodoEUrl(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, r.Method + " " + r.URL.String())
	return 
}
