package main

import ( "net/http"
		 "log"
		 "io/ioutil"
		 "encoding/json"
		 "github.com/gorilla/mux"
		 "time"
		 "strconv"
		)


type User struct {
	Email  	  string `json:email`
	Pass   	  string `json:pass` 
	Birthdate string `json:birthdate`
}

var registrados []User = make([]User, 25)

var messageHeader string = `{
	"message" : "invalid header"
}`

var messageBody string = `{
	"message" : "invalid body"
}`

var messageSuccessfullSignup string = `{
	"message" : "user succesfully registered"
}`

func main() {

	StartServer()
	
	// Essauinha deve ser executada sem alteração
	// da função StartServer	
	log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/signup", Signup)
	r.HandleFunc("/signin", Signin)

	server := &http.Server{
			Addr       : "172.22.87.27:8082",
			IdleTimeout: duration,
			Handler    : r, 
	}

	log.Print(server.ListenAndServe())
}

func Signup(res http.ResponseWriter, req *http.Request) {

	var u User
	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)

	if(VerificaMetodoEHeader(res, req)){
		if (u.Email == "" || u.Pass == "" || u.Birthdate == ""){
			res.Header().Set("Status", "400")
			res.Write([]byte(messageBody))
		}else{
			registrados = append(registrados, u)
			res.Header().Set("Status", "200")
			res.Write([]byte(messageSuccessfullSignup))
		}
	}
}

func Signin(res http.ResponseWriter, req *http.Request){

	var u User
	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)
	usuarioExiste, usuario := EncontreUsuario(registrados, u)

	if(VerificaMetodoEHeader(res, req)){
		if (u.Email == "" ||u.Pass == ""){
			res.Header().Set("Status", "400")
			res.Write([]byte(messageBody))

		}else if (!usuarioExiste){
			res.Header().Set("Status", "403")

		}else if (usuarioExiste){
			idade := CalculaIdade(usuario.Birthdate)
			res.Write([]byte(
				`{
	"message" : "user succesfully logon",
	"idade" : ` + idade + `
}`))
		}
	}
}

func EncontreUsuario(registrados []User,u User) (bool, User){
	for i := range registrados {
	    if (registrados[i].Email ==u.Email && registrados[i].Pass ==u.Pass) {
			return true, registrados[i];
		}
	}
	return false, u;
}

func CalculaIdade(birthdate string) (string){
	dataAtual := time.Now().String()
	anoAtual, _ := strconv.Atoi(dataAtual[0:4])
	anoNasc, _ := strconv.Atoi(birthdate[6:10])
	idade := anoAtual - anoNasc
	return strconv.Itoa(idade)
}

func VerificaMetodoEHeader(res http.ResponseWriter, req *http.Request)(bool){
	metodoUtilizado := req.Method 
	headers := req.Header
	expectedHeader := headers["Content-Type"]

	if (metodoUtilizado != "POST"){
		res.Header().Set("Status", "405")
		return false
	}else if expectedHeader[0] != "application/json"{
		res.Header().Set("Status", "400")
		res.Write([]byte(messageHeader))
		return false
	}
	return true
}