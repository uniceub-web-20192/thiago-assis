package main

import ( "net/http"
		 "log"
		 "time" 
		 "github.com/gorilla/mux"
		 "io/ioutil"
		 "encoding/json")

type User struct {

	Name   string `json:name`
	Pass   string `json:pass` 
	Gender string `json:gender`
	Birthdate string `json:birthdate`
	Email  string `json:email`
}


func main() {

	StartServer()
	
	// Execute essa linha sem alterar a função StartServer

	log.Println("[INFO] Servidor no ar!")
}

func StartServer() {

	duration, _ := time.ParseDuration("1000ns")

	r := mux.NewRouter()

	r.HandleFunc("/signup", Signup).Methods("POST").HeadersRegexp("Content-Type", "application/json")

	r.HandleFunc("/signin", Signin).Methods("POST").HeadersRegexp("Content-Type", "application/json")

	server := &http.Server{
			Addr       : "172.22.51.127:8092",
			IdleTimeout: duration,
			Handler    : r, 
	}

	log.Print(server.ListenAndServe())
}

func Signup(res http.ResponseWriter, req *http.Request) {

	var u User

	body, _ := ioutil.ReadAll(req.Body)
	
	json.Unmarshal(body, &u)
	userjson,_ := json.Marshal(&u)
	res.Write([]byte(userjson))


}


func Signin(res http.ResponseWriter, req *http.Request){

	var u User
	body, _ := ioutil.ReadAll(req.Body)
	json.Unmarshal(body, &u)
	usuarioExiste, usuario := Usuario(registrados, u)

	if(Metodo(res, req)){
		if (u.Email == "" ||u.Pass == ""){
			res.Header().Set("Status", "400")
			res.Write([]byte(messageBody))

		}else if (!usuarioExiste){
			res.Header().Set("Status", "403")

		}else if (usuarioExiste){
			idade := Idade(usuario.Birthdate)
			res.Write([]byte(
				`{
	"messagem" : "Usuario adicionado",
	"idade" : ` + idade + `
}`))
		}
	}
}

func Usuario(registrados []User,u User) (bool, User){
	for i := range registrados {
	    if (registrados[i].Email ==u.Email && registrados[i].Pass ==u.Pass) {
			return true, registrados[i];
		}
	}
	return false, u;
}


func Idade(birthdate string) (string){
	dataAtual := time.Now().String()
	anoAtual, _ := strconv.Atoi(dataAtual[0:4])
	anoNasc, _ := strconv.Atoi(birthdate[6:10])
	idade := anoAtual - anoNasc
	return strconv.Itoa(idade)
}

func Metodo(res http.ResponseWriter, req *http.Request)(bool){
	metodoUtilizado := req.Method 
	headers := req.Header
	expectedHeader := headers["Content-Type"]

	if (metodoUtilizado != "POST"){
		res.Header().Set("Status", "405")
		return false
	}else if expectedHeader[0] != "application/json"{
		res.Header().Set("Status", "400")
		res.Write([]byte(messageHeader))
		return false
	}
	return true
}
